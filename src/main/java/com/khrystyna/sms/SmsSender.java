package com.khrystyna.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

/**
 * An instance of this class is used to send SMS messages
 * by twillio library.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 18.04.2019
 */
class SmsSender {
    private static final String ACCOUNT_SID = "ACf2f08fa84a949d44719caad28a886ec4";
    private static final String AUTH_TOKEN = "86e1f1217ecae6ce2a56b256ffba4363";

    /**
     * Sends message by sms.
     *
     * @param sms message to send
     */
    static void send(String sms) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Message.creator(new PhoneNumber("+380637315152"),
                new PhoneNumber("+12512458073"), sms)
                .create();
    }
}