package com.khrystyna.sms;

import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import java.io.Serializable;
import java.util.Date;

/**
 * An instance of this class is used to create
 * appender for SMS notifications.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 18.04.2019
 */
@Plugin(name = "SmsAppender",
        category = Core.CATEGORY_NAME,
        elementType = Appender.ELEMENT_TYPE,
        printObject = true)
public class SmsAppender extends AbstractAppender {

    private static volatile SmsAppender instance;

    private SmsAppender(
            @PluginAttribute("name") String name,
            @PluginElement("layout") Layout<? extends Serializable> layout,
            @PluginElement("filter") Filter filter,
            @PluginAttribute("ignoreExceptions") boolean ignoreExceptions,
            @PluginElement("properties") Property[] properties

    ) {
        super(name, filter, layout, ignoreExceptions, null);
    }

    @PluginFactory
    public static SmsAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("layout") Layout<? extends Serializable> layout,
            @PluginElement("filters") Filter filter,
            @PluginAttribute("ignoreExceptions") boolean ignoreExceptions,
            @PluginElement("properties") Property[] properties
    ) {
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        instance = new SmsAppender(name, layout, filter,
                ignoreExceptions, properties);
        return instance;
    }

    public static SmsAppender getInstance() {
        return instance;
    }

    @Override
    public void append(LogEvent logEvent) {
        SmsSender.send("\n[" + logEvent.getLevel().toString() + "] " +
                new Date(logEvent.getTimeMillis()).toString() + " "
                + logEvent.getSource().toString() + " "
                + logEvent.getMessage().getFormattedMessage());
    }
}
