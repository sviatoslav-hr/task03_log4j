package com.khrystyna;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Program main start point.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 18.04.2019
 */
public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);


    /**
     * Main method that runs when the program is started.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        testLogger();
    }

    /**
     * Sends messages for logger on different levels to test how logger works.
     */
    private static void testLogger() {
        logger.trace("Trace message");
        logger.debug("Debug message");
        logger.info("Info message");
        logger.warn("Warn message");
        logger.error("Error message");
        logger.fatal("Fatal message");
    }
}
